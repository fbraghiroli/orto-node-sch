# Orto-node schematics

This repo contains schematics for the orto-node project (checkout orto-node repo for firmware source code).

It uses the stm32 bluepill board, an nrf24l01 module and a li-ion usb battery charger module (which contains also overcurrent and overdischarge protections).

With this board we have 10 analog configured in this way:

 * 5 channels for soil moisture sensors
 * 5 channels for photoresistors

Soil moisture sensors have an on-board 3.3V thus can be powered using the battery voltage (through the li-ion chanrger module). However to power them off (and save energy), their power line is controlled through a generic npn transistor. The output of these sensors are an analog signal from 0V to 3.3V.

Photoresistors used are gl5528 which have a light resistance (10Lux) of 10-20 Ohms.
These components requires regulated power (to allow precision measurements) which is provided by one of the gpio output of the board (used also as an on/off switch).
Setting up a voltage divider with a 10K, we got ~0.3mA per photoresistor which means about 1.5mA of current required by a single gpio pin.

To power up everything it is used a 18650 cell recovered from a battery pack of a dead laptop.